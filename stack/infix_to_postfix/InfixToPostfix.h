#pragma once
#include <string>
#include "Stack.h"
class Convert {
	int rank=0;
	Stack _stack;
	int subRank(const char character)const noexcept;
	int stackPriority(const char character)const noexcept;
	int inPriority(const char character)const noexcept;
	bool isOperand(const char character)const noexcept;
public:
	int infixToPostfix(const std::string infix, std::string& postfix);
};
#pragma once
class Stack
{
	struct Node
	{
		char data;
		Node* next;
	};
	Node *Tos;
public:
	Stack()noexcept;
	int push(char& element);
	int pop(char* element);
	bool isEmpty()const noexcept;
	char getElementHead()const noexcept;
};
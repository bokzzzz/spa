#include "InfixToPostfix.h"

int Convert::infixToPostfix(const std::string infix, std::string& postfix)
{
	char x;
	for(char next:infix)
	{
		if (isOperand(next))
		{
			postfix+= next;
			rank++;
		}
		else
		{

			while (!_stack.isEmpty() && inPriority(next) <= stackPriority(_stack.getElementHead()))
			{
				_stack.pop(&x);
				postfix += x;
				rank += subRank(x);
				if (rank < 1)
					return 0;
			}
			if ( _stack.isEmpty() || next != ')')
				_stack.push(next);
			else
				_stack.pop(&x);
		}
	}
	while (_stack.pop(&x))
		postfix += x;
	postfix += '\0';
	return 1;
}


int Convert::subRank(const char character) const noexcept
{
	switch (character)
	{
	case '+':
	case '-':
	case '*':
	case '/':
	case '^':
		return -1;
	default:
		return 0;
	}

}

int Convert::stackPriority(const char character) const noexcept
{
	switch (character)
	{
	case '+':
	case '-':
		return 2;
	case '*':
	case '/':
		return 3;
	case '^':
		return 4;
	case '(':
		return 0;
	default:
		return -1;
	}

}

int Convert::inPriority(const char character) const noexcept
{
	switch (character)
	{
	case '+':
	case '-':
		return 2;
	case '*':
	case '/':
		return 3;
	case '^':
		return 5;
	case '(':
		return 6;
	case ')':
		return 1;
	default:
		return -1;
	}

}

bool Convert::isOperand(const char character) const noexcept
{
	switch (character)
	{
	case '+':
	case '-':
	case '*':
	case '/':
	case '^':
	case '(':
	case ')':
		return false;
	default: return true;
	}
}

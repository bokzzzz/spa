#include "InfixToPostfix.h"
#include <string>
#include <iostream>

void main()
{
	std::string postfix, infix;
	Convert* convert = new Convert();
	std::cout << "Unesite infix-ni izraz: ";
	std::cin >> infix;
	convert->infixToPostfix(infix, postfix);
	std::cout << "Postfixni izraz je: " << postfix << std::endl;
}
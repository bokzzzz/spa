#include "CheckBracket.h"


bool CheckBracket::isBracketOk(std::string& str) noexcept
{
	for (char c : str)
	{
		if (c == '(' || c == '[' || c == '{') _stack.push(c);
		if (c == ')' || c == ']' || c == '}')
		{
			char p;
			if(!(_stack.pop(&p))) return false;
			if ((c==')' && p!='(' ) || (c == ']' && p != '[') || (c == '}' && p != '{')) return false;
		}
	}
	if (!_stack.isEmpty()) return false;
	return true;
}

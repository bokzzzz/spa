#pragma once
#include "Stack.h"
#include <string>
class CheckBracket
{
	Stack _stack;
public:
	bool isBracketOk(std::string& str)noexcept;
};
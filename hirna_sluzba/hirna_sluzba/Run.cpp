
#include "HitnaSimulacija.h"
#include <iostream>
#include <string>
void main()
{
	ambulanceServicesSimulation *am = new ambulanceServicesSimulation();
	int numberOfPatient;
	std::string ulaz;
	do
	{
		std::cout << "Unesite broj pacijenata[1-8]: ";
		std::cin >> numberOfPatient;
	} while (numberOfPatient < 1 || numberOfPatient >9);
	do
	{
		std::cout << "Unesite START da bi se simulacija pokrenula: ";
		std::cin >> ulaz;
	}
	while (ulaz.compare("START"));
	am->start(numberOfPatient);
}
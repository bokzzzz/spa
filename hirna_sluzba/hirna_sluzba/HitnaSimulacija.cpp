#define _CRT_SECURE_NO_WARNINGS
#include "HitnaSimulacija.h"
#include <cstdlib>
#include <iostream>
#include <thread>
#include <chrono>
#include <random>
#include <iomanip>
#include <string>
void ambulanceServicesSimulation::start(int & _numberOfPatient) noexcept
{
	int randomComplete, randomPriority, randomOfArr;
	for (int i = 0; i < _numberOfPatient; i++)
	{
		std::random_device rd;
		std::mt19937 rng(rd());
		std::uniform_int_distribution<int> uni(1, 30);
		std::uniform_int_distribution<int> uni2(1, 4);
		randomComplete = uni(rng);
		randomOfArr = uni(rng);
		randomPriority = uni2(rng);
		Patient *patient = new Patient(randomOfArr, randomComplete, randomPriority);
		prQueue.enqueue(*patient);
		delete patient;
	}
	std::cout << prQueue;
	int i = 1;
	std::cout << "\n";
	while (!prQueue.isEmpty())
	{
		Patient *patient;
		prQueue.dequeue(&patient);
		time_t t = time(0); 
		struct tm * now = localtime(&t);
		int timete = patient->timeToComplete - 1;
		std::cout << i << ". pacijent je usao u: " << now->tm_hour << ":" << now->tm_min << ":" << now->tm_sec << " i pregled traje: " << patient->timeToComplete << " i traje jos: " << std::setfill('0') << std::setw(2) << patient->timeToComplete;
		while (timete >= 0)
		{
			if (timete < 10)
			{
				std::this_thread::sleep_for(std::chrono::seconds(1));
				std::cout << "\b\b" << std::setfill('0') << std::setw(2) << timete--;
			}
			else if (timete > 9 && timete < 31)
			{
				std::this_thread::sleep_for(std::chrono::seconds(1));
				std::cout << "\b\b" << timete--;
			}
		}
		std::this_thread::sleep_for(std::chrono::seconds(1));
		std::cout << std::endl << i++ << ". pacijent je zavrsio pregled!" << std::endl;
		delete patient;
	}
	std::cout << std::endl << "Simulacija je zavrsena!" << std::endl;
	std::cout << "KRAJ SIMULACIJE, UNESITE Q ZA IZLAZ. ";
	std::string izlaz;
	do
	{
		std::cin >> izlaz;
	} while (izlaz.compare("Q"));
}

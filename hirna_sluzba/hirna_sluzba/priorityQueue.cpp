#include "priorityQueue.h"

priorityQueue::priorityQueue()noexcept:head(nullptr),tail(nullptr){}

bool priorityQueue::enqueue(Patient _patient)
{
	Node* _node;
	if (_node = new Node(_patient))
	{
		if (!head)
		{
			head = tail = _node;
			return true;
		}
		Node* tmp = head,*prev=head;
		for (; tmp && (tmp->data.priority < _node->data.priority || (tmp->data.timeOfArrival <= _node->data.timeOfArrival && tmp->data.priority == _node->data.priority));)
		{
			prev = tmp;
			tmp = tmp->next;
		}
		if (!tmp)
		{
			tail->next = _node;
			tail = _node;
			return true;
		}
		if (tmp == head)
		{
			_node->next = head;
			head = _node;
			return true;
		}
		_node->next = prev->next;
		prev->next = _node;
	}
	return false;
}

bool priorityQueue::dequeue(Patient ** _patient)
{
	if (!head) return false;
	if (head == tail)
	{
		*_patient = &(head->data);
		head = tail = nullptr;
		return true;
	}
	*_patient = &(head->data);
	head = head->next;
	return true;
}

bool priorityQueue::isEmpty() const noexcept
{
	return head ? false:true;
}

void priorityQueue::print()const
{
	Node *tmp = head;
	for (int i=1; tmp; tmp = tmp->next)
	{
		std::cout << std::endl <<i++ <<". pacijent: "<<"Vrijeme dolaska: " << tmp->data.timeOfArrival << ", vrijeme trajanja pregleda: " << tmp->data.timeToComplete << ", prioritet je: " << tmp->data.priority;
	}
	std::cout << std::endl;
}


std::ostream& operator<<(std::ostream& str, const priorityQueue& que)
{
	que.print();
	return str;
}

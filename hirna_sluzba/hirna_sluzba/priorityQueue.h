#pragma once
#include "Patient.h"
#include <iostream>

class priorityQueue
{
	struct Node
	{
		Node(Patient _patient):data(_patient),next(nullptr){}
		Patient data;
		Node* next;
	};
	Node* head, *tail;
	friend std::ostream& operator<<(std::ostream& str, const priorityQueue& que);
	void print()const;
public:
	priorityQueue()noexcept;
	bool enqueue(Patient _patient);
	bool dequeue(Patient** _patient);
	bool isEmpty()const noexcept;
};
#pragma once
#include "defines.h"
#include <stdio.h>
#include <stdlib.h>

void compress(char A, int num)
{
	int i = 0;
	while (i < num)
	{
		if (table[i].letter == A)
		{
			printf("%s", table[i].code);
			return;
		}
		++i;
	}
	printf("***\n\nGreska.\n%c ne postoji u tabeli\n", A);
	exit(-1);
}

#include "buildHuffmanTree.h"
#include "compress.h"
#include "decompress.h"
#include <cstdio>
#include <math.h>

int main(int argc, char **argv)
{
	if (argc < 2)
	{
		printf("CMD execution error.");
		return -1;
	}
	char buffer[128];
	tip freq[MAX], procenat = 0;
	FILE *input;
	if ((input = fopen(argv[1], "r")) == NULL)
		return -1;
	fgets(buffer, 128, input);
	int i = 0, j = 0;
	double stoo = 100;
	while (buffer[i])
	{
		if (buffer[i] >= 'A' && buffer[i] <= 'Z')
			buffer[j++] = buffer[i];
		++i;
	}
	buffer[j] = 0;
	printf("\nUcitani znakovi: \n");
	for (i = 0; i < j; i++)
	{
		fscanf(input, "%lf", freq + i);
		printf("%c:%lf\n",buffer[i],freq[i]);
		procenat += freq[i];
	}
	fclose(input);
	if ((round(procenat*100)/100) != (round(stoo * 100) / 100))
		return	printf("Vjerovatnoca nije 100.\n");
	table = (struct codeTable *)malloc(j * sizeof(struct codeTable));
	buildHuffmanTree(buffer, freq, j);
	int q = 1;
	while (q) {
		printf("\nCompress/Decompres/End?(C / D / Q): ");
		switch (getchar())
		{
		case 'C': case 'c':
			printf("Unesite tekst za kompresiju: ");
			scanf("%s", buffer);
			for (i = 0; buffer[i]; compress(buffer[i++], j));
			getchar();
			break;
		case 'D': case 'd':
			printf("Unesite tekst za dekompresiju: ");
			scanf("%s", buffer);
			for (i = 0; buffer[i]; i += decompress(buffer + i, MAX - i));
			getchar();
			break;
		case 'q':case 'Q':
			q = 0; break;
		default:
			printf("Pogresan unos!\n");
		}
	}
	free(table);
	return 0;
}

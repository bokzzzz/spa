#pragma once
#define _CRT_SECURE_NO_WARNINGS
#define MAX 27

typedef double tip;

typedef struct _node
{
	tip value;
	int visited;
	char letter[MAX];
	struct _node *left, *right;
} Node;

struct codeTable
{
	char letter, code[MAX];
} *table;

int tableSize = 0;
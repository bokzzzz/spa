#pragma once
#include "defines.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void inOrder(Node *tree, char *code)
{
	if (tree->left)
	{
		char tempL[MAX], tempR[MAX];
		strcpy(tempL, code);
		strcpy(tempR, code);
		inOrder(tree->left, strcat(tempL, "0"));
		inOrder(tree->right, strcat(tempR, "1"));
	}
	else
	{
		printf("%s: %s\n", tree->letter, code);
		table[tableSize].letter = tree->letter[0];
		strcpy(table[tableSize++].code, code);
	}
	free(tree);
}
#pragma once
#include "defines.h"
#include <stdio.h>
#include <string.h>

int decompress(char *buff, int num)
{
	char temp[MAX] = { 0 };
	for (int i = 0; i < num; ++i)
	{
		temp[i] = buff[i];
		for (int j = 0; j < tableSize; j++)
			if (!strcmp(temp, table[j].code))
			{
				printf("%c", table[j].letter);
				return i + 1;
			}
	}
	printf("***\nGreska... Provjerite unos.\n");
	exit(-1);
}
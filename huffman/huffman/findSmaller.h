#pragma once
#include "defines.h"
#include <stdio.h>

int findSmaller(Node * array[], int differentFrom, int num)
{
	int smaller = 0;
	while (array[smaller]->visited || smaller == differentFrom)
		++smaller;
	for (int i = smaller + 1; i < num; i++)
	{
		if (array[i]->visited || i == differentFrom)
			continue;
		if (array[i]->value < array[smaller]->value)
			smaller = i;
	}
	return smaller;
}

#pragma once
#include "findSmaller.h"
#include "inOrder.h"
#include <stdlib.h>
#include <string.h>

void buildHuffmanTree(char * arr, tip * freq, int num)
{
	Node *temp;
	Node **array = (Node **)malloc(num * sizeof(Node *));
	int i, subTrees = num;
	int smallOne, smallTwo;
	for (i = 0; i < num; i++)
	{
		array[i] = (Node *)malloc(sizeof(Node));
		array[i]->value = freq[i];
		array[i]->letter[0] = arr[i];
		array[i]->letter[1] = 0;
		array[i]->left = NULL;
		array[i]->visited = 0;
		array[i]->right = NULL;
	}
	while (subTrees > 1)
	{
		smallOne = findSmaller(array, -1, num);
		smallTwo = findSmaller(array, smallOne, num);
		temp = array[smallOne];
		array[smallOne] = (Node *)malloc(sizeof(Node));
		array[smallOne]->value = temp->value + array[smallTwo]->value;
		strcpy(array[smallOne]->letter, temp->letter);
		strcat(array[smallOne]->letter, array[smallTwo]->letter);
		temp->visited = 1; array[smallTwo]->visited = 1;
		array[smallOne]->visited = 0;
		array[smallOne]->right = array[smallTwo];
		array[smallOne]->left = temp;
		--subTrees;
	}
	printf("\nGenerisani kodovi: \n");
	inOrder(array[smallOne], "");
	return;
}
